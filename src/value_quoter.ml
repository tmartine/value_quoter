let deriver = "quote"

let core_type_of_decl ~options:_ ~path:_ type_decl =
  let typ = Ppx_deriving.core_type_of_type_decl type_decl in
  let loc = type_decl.ptype_loc in
  Ppx_deriving.poly_arrow_of_type_decl
    (fun var ->
      [%type: ?loc:Location.t -> [%t var] -> Parsetree.expression])
    type_decl
    [%type: ?loc:Location.t -> [%t typ] -> Parsetree.expression]

let build_expr ~loc pexp_desc : Parsetree.expression =
  [%expr { Parsetree.pexp_loc = loc; pexp_attributes = [];
    pexp_desc = [%e pexp_desc]}]

let build_type ~loc ptyp_desc : Parsetree.expression =
  [%expr { Parsetree.ptyp_loc = loc; ptyp_attributes = [];
    ptyp_desc = [%e ptyp_desc]}]

let build_apply ~loc f x : Parsetree.expression =
  [%expr [%e f] [%e x]]

let expression_of_string ~loc s : Parsetree.expression =
  Ast_helper.Exp.constant ~loc (Ast_helper.Const.string s)

let build_list ~loc expr_list : Parsetree.expression =
  List.fold_right
    (fun term list : Parsetree.expression ->
      [%expr [%e term] :: [%e list]]) expr_list [%expr []]

let attr_nobuiltin attrs =
  Ppx_deriving.(attrs |> attr ~deriver "nobuiltin" |> Arg.get_flag ~deriver)

let attr_opaque attrs =
  Ppx_deriving.(attrs |> attr ~deriver "opaque" |> Arg.get_flag ~deriver)

let rec build_longident ~loc (longident : Longident.t) : Parsetree.expression =
  match longident with
  | Lident ident ->
      [%expr Longident.Lident [%e expression_of_string ~loc ident]]
  | Ldot (lhs, field) ->
      let lhs = build_longident ~loc lhs in
      let field = expression_of_string ~loc field in
      [%expr Longident.Ldot ([%e lhs], [%e field])]
  | Lapply (lhs, rhs) ->
      let lhs = build_longident ~loc lhs in
      let rhs = build_longident ~loc rhs in
      [%expr Longident.Lapply ([%e lhs], [%e rhs])]

let type_declaration_of_structure_item (stri : Parsetree.structure_item) =
  match stri.pstr_desc with
  | Pstr_type (_, [type_declaration]) -> type_declaration
  | _ -> assert false

let build_record ~loc field_list : Parsetree.expression =
  build_expr ~loc [%expr Parsetree.Pexp_record ([%e build_list ~loc (List.map (fun (label, value) : Parsetree.expression -> [%expr ({ Location.loc; txt = [%e build_longident ~loc label]}, [%e value])]) field_list)], None)]

let rec expr_of_record ~loc ~quoter binder (label_declarations : Parsetree.label_declaration list) :
    Parsetree.expression =
  label_declarations |> List.map begin
    fun (field : Parsetree.label_declaration) : (Longident.t * Parsetree.expression) ->
      let label = Longident.Lident field.pld_name.txt in
      (label, [%expr
		 [%e expr_of_type quoter field.pld_type] ?loc:(Some loc)
		 [%e Ast_helper.Exp.field (Ast_helper.Exp.ident ~loc { loc; txt = Lident binder }) { loc; txt = label }]])
  end |>
  build_record ~loc
and expr_of_tuple ~loc ~quoter types =
  let sub_cases = types |> List.mapi @@
    fun index core_type : (Parsetree.pattern * Parsetree.expression) ->
      let binder = Printf.sprintf "arg%d" index in
      Ast_helper.Pat.var ~loc { loc; txt = binder },
      [%expr
	 [%e expr_of_type quoter core_type] ?loc:(Some loc)
	 [%e Ast_helper.Exp.ident ~loc { loc; txt = Lident binder }]] in
  let binders, sub_terms = List.split sub_cases in
  let binders =
    match binders with
    | [binder] -> binder
    | _ -> Ast_helper.Pat.tuple binders in
  let sub_terms : Parsetree.expression =
    match sub_terms with
    | [sub_term] -> sub_term
    | _ ->
	let sub_terms_list = build_list ~loc sub_terms in
	build_expr ~loc [%expr Parsetree.Pexp_tuple [%e sub_terms_list]] in
  binders, sub_terms
and expr_of_type quoter (core_type : Parsetree.core_type) :
    Parsetree.expression =
  let loc = core_type.ptyp_loc in
    let opaque = attr_opaque core_type.ptyp_attributes in
    if opaque then
      [%expr fun ?(loc = Location.none) x -> [%e build_expr ~loc [%expr Parsetree.Pexp_extension ({ loc; txt = "opaque" }, PStr [])]]]
    else
      let builtin = not (attr_nobuiltin core_type.ptyp_attributes) in
      match builtin, core_type with
      | true, [%type: int] ->
          [%expr fun ?(loc = Location.none) x ->
	    [%e build_expr ~loc
	       [%expr Parsetree.Pexp_constant
	          (Parsetree.Pconst_integer (string_of_int x, None))]]]
      | true, [%type: string] ->
          [%expr fun ?(loc = Location.none) x ->
	    [%e build_expr ~loc
	       [%expr Parsetree.Pexp_constant
	          (Parsetree.Pconst_string (x, None))]]]
      | true, [%type: float] ->
          [%expr fun ?(loc = Location.none) x ->
	    [%e build_expr ~loc
	       [%expr Parsetree.Pexp_constant
	          (Parsetree.Pconst_float (string_of_float x, None))]]]
      | true, [%type: bool] ->
          value_quoter_of_type_decl ~quoter ~loc
            (type_declaration_of_structure_item [%stri type bool = false | true])
      | true, [%type: [%t? t] option] ->
          build_apply ~loc
            (value_quoter_of_type_decl ~quoter ~loc
	       (type_declaration_of_structure_item
                  [%stri type 'a option = None | Some of 'a])) (expr_of_type quoter t)
      | true, [%type: ([%t? ok], [%t? error]) result] ->
          build_apply ~loc
            (build_apply ~loc
               (value_quoter_of_type_decl ~quoter ~loc
	          (type_declaration_of_structure_item
                     [%stri type ('a, 'b) result = Ok of 'a | Error of 'b]))
               (expr_of_type quoter ok))
            (expr_of_type quoter error)
      | true, [%type: [%t? t] list] ->
          [%expr
            let rec quote_list quote_arg list =
              match list with
              | [] -> [%e build_expr ~loc [%expr Pexp_construct ({ loc; txt = Lident "[]" }, None)]]
              | hd :: tl -> [%e build_expr ~loc [%expr Pexp_construct ({ loc; txt = Lident "::" }, Some ([%e build_expr ~loc [%expr Pexp_tuple [quote_arg ?loc:(Some loc) hd; quote_list quote_arg tl]]]))]] in
            fun ?(loc = Location.none) -> quote_list [%e expr_of_type quoter t]]
      | _, { ptyp_desc = Ptyp_tuple types; _ } ->
          let binders, expr = expr_of_tuple ~loc ~quoter types in
          [%expr fun ?(loc = Location.none) [%p binders] -> [%e expr]]
      | _, { ptyp_desc = Ptyp_constr ({ txt = lid; _ }, args); _ } ->
          let quote_fn =
	    Ast_helper.Exp.ident { loc;
	                           txt = Ppx_deriving.mangle_lid (`Prefix deriver) lid } in
          let fwd =
	    List.fold_left (build_apply ~loc)
	      (Ppx_deriving.quote ~quoter quote_fn)
	      (List.map (expr_of_type quoter) args) in
          [%expr [%e fwd]]
      | _, { ptyp_desc = Ptyp_var name; _ } ->
          Ast_helper.Exp.ident { loc; txt = Lident ("poly_" ^ name) }
      | _ -> failwith (Format.asprintf "unimplemented: %a" Pprintast.core_type core_type)
and value_quoter_of_type_decl ~quoter ~loc (type_decl : Parsetree.type_declaration) :
    Parsetree.expression =
  let value_quoter : Parsetree.expression =
    match type_decl.ptype_kind with
    | Ptype_abstract ->
	begin
	  match type_decl.ptype_manifest with
	  | Some manifest -> [%expr [%e expr_of_type quoter manifest] ~loc value]
	  | None ->
	      Ppx_deriving.raise_errorf ~loc
		"%s cannot be derived for abstract types" deriver
	end
    | Ptype_variant constrs ->
	let cases =
	  constrs |> List.map @@ fun constructor_declaration ->
	    let { pcd_name; pcd_args; pcd_loc = loc; _ } : Parsetree.constructor_declaration =
	      constructor_declaration in
	    let args, (body : Parsetree.expression) =
	      match pcd_args with
              | Pcstr_tuple [] ->
                  None, [%expr None]
	      | Pcstr_tuple types ->
                  let binders, expr = expr_of_tuple ~loc ~quoter types in
                  Some binders, [%expr Some [%e expr]]
	      | Pcstr_record label_declarations ->
		  let binder = "arg" in
		  let sub_terms = expr_of_record ~loc ~quoter binder label_declarations in
		  Some (Ast_helper.Pat.var { loc; txt = binder }), [%expr Some [%e sub_terms]] in
	    let body = build_expr ~loc [%expr Parsetree.Pexp_construct ({ loc; txt = Lident [%e expression_of_string ~loc pcd_name.txt]}, [%e body])] in
	    Ast_helper.Exp.case
	      (Ast_helper.Pat.construct ~loc { loc; txt = Lident pcd_name.txt } args) body in
        Ast_helper.Exp.match_ ~loc [%expr value] cases
    | Ptype_record label_declarations ->
	let binder = "value" in
	expr_of_record ~loc ~quoter binder label_declarations
    | _ -> failwith "unimplemented" in
  let type_name =
    Ast_helper.Exp.constant ~loc
      (Ast_helper.Const.string type_decl.ptype_name.txt) in
  let type_args =
    build_list ~loc
      (List.map (fun _ -> build_type ~loc [%expr Ptyp_any]) type_decl.ptype_params) in
  let value_quoter : Parsetree.expression =
    [%expr fun ?(loc = Location.none) value ->
      [%e build_expr ~loc [%expr Pexp_constraint ([%e value_quoter],
        [%e build_type ~loc [%expr Ptyp_constr ({ loc; txt = Lident [%e type_name]}, [%e type_args])]])]]] in
  let polymorphize = Ppx_deriving.poly_fun_of_type_decl type_decl in
  polymorphize value_quoter

let str_of_type ~options ~path (type_decl : Parsetree.type_declaration) =
  let loc = type_decl.ptype_loc in
  let quoter = Ppx_deriving.create_quoter () in
  let out_type =
    Ppx_deriving.strong_type_of_type @@
      core_type_of_decl  ~options ~path type_decl in
  let quote_var =
    Ast_helper.Pat.var ~loc { loc; txt = Ppx_deriving.mangle_type_decl (`Prefix deriver) type_decl } in
  let value_quoter = value_quoter_of_type_decl ~quoter ~loc type_decl in
  [Ast_helper.Vb.mk ~attrs:[Ppx_deriving.attr_warning [%expr "-39"]]
         (Ast_helper.Pat.constraint_ quote_var out_type)
         (Ppx_deriving.sanitize ~quoter value_quoter)]

let derive_type_decl_str ~options ~path type_decls =
  [Ast_helper.Str.value Recursive
    (List.concat (List.map (str_of_type ~options ~path) type_decls))]

let sig_of_type ~options ~path type_decl =
  [Ast_helper.Sig.value (Ast_helper.Val.mk (Location.mknoloc (Ppx_deriving.mangle_type_decl (`Prefix deriver) type_decl))
     (core_type_of_decl ~options ~path type_decl))]

let derive_type_decl_sig ~options ~path type_decls =
  List.concat (List.map (sig_of_type ~options ~path) type_decls)

let () =
  Ppx_deriving.(register (create deriver
    ~core_type: (Ppx_deriving.with_quoter expr_of_type)
    ~type_decl_str: derive_type_decl_str
    ~type_decl_sig: derive_type_decl_sig ()))
